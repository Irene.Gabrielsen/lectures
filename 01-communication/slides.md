theme: moon
title: INF112 Innføring i systemutvikling
cr_word: INF112 22V
cr_color: #1d1f20
use_menu: true
menu_bottom: false
transition: none
---right
## Innføring i systemutvikling
<img style="height: 75vh" src="img/life-software-engineer.jpg">


---notes 
- Intro: Jobber til vanlig som utvikler, foreleser på deltid. 
- arbeidshverdagen min har endret seg vesentlig ila 2020. Samtidig skal
  output (systemer levert) være likt. 
- Hvordan var hverdagen? Videomøter (jobbe sammen remote eller diskutere
  problemstillinger), team på samme rom med tavle og skjermer. 
- Nå? Alle remote, skjermdeling og video/tale eneste måte å kommunisere på
- kommunikasjon som faktor for trivsel og effektivitet 
- Dere skal lære å utvikle systemer, og dere må også i alle fall delvis gjøre
  dette med remote-arbeid
- intro Siv: ansatt 20% for å forelese dette faget, jobber ellers som konsulent
  i kantega. Arrangerte også Nerdschool.
- Konsulent: ute hos kunde, samarbeide med kunde, remote helt eller delvis,
  mange ulike prosjekter
- Dette faget handler om verkøyene vi bruker for å utvikle programvare
- Hvordan bruker vi disse verktøyene på en god måte?
- Når brukes de ulike verktøyene? 

---down
## Software Engineering
---fragment
<img style="height: 22vh" src="img/se-silly.jpeg">
---fragment
<img style="height: 22vh" src="img/se-messy.jpeg">
---fragment
<img style="height: 25vh" src="img/hamilton.jpeg">

---notes
- hvordan håndtere noe som er:
   - for stort til at du har oversikt
   - for stort til at du kan gjøre det alene
   - hvor du mangler informasjon
   - og du gjør feil, roter det til
   - og du skulle vært ferdig i går

---down
## Software Engineering

Ikke «programmerings-gud» – men:

* fungere bra sammen med andre,
* bruke det du kan
* kunne lære mer

## Samarbeid
<img style="height: 60vh" src="img/hands_collaboration.jpeg">

---notes
- Dette faget handler om hvordan vi jobber, og mer viktig: jobber sammen for å utvikle programvare
- Kommunikasjon er det viktigste verktøyet vi har, derfor den lille historien 
- Hvordan kommunisere i forelensingen? Vil du si eller spørre om noe, rekk opp
  hånden eller skriv en kommentar (enten til meg eller felles)

---down
## Håndverk
<img style="height: 60vh" src="img/clay_craft.jpeg">

---notes 
- Heter innføring i systemutvikling, men kunne også vært kalt utviklingshåndverk
- Litt som sjakk: lære grunnreglene er enkelt, spille på en god måte: vanskelig
- Lære om håndverket gjennom forelesninger og prosjektarbeid i team
- Praktisk fag, praktisk fokus

--down
### Hvordan skal vi jobbe med faget?

* Forelesninger
* Les pensum, så du blir klok og vis
* Oppgaver, så du blir bedre håndverker
* Obliger (teamarbeid) – vis hva du lærer (50% av karakteren)
    * Du må bestå *både obligatoriske arbeidskrav og eksamen for å stå i faget*

---notes

- Forelesninger i første halvdel av kurset, prosjektarbeid resten
- Oppgaver for å lære verktøy og å lære seg å jobbe sammen og bruke ulike
  verktøy
- Totalt fire obliger, disse angir 50% av poengene før karaktergiving. Dere skal lage et
  system, og alle innleveringene omhandler dette systemet. Begynner om noen
  uker.  


---down
### Praktisk informasjon

- Meld dere på grupper selv – påmelding åpner snart
- Gruppene er faste
- Undervisningsassistent: Olav Røthe Bakken
- Gruppeledere: Alba, Jørgen, Olav, Peter, Sindre og Vetle

---notes
- Påmelding kommer – må ordne Zoom-løsning
- Alle jobber i team omtrent hele semesteret. 
- Dere blir tildelt team innenfor den gruppen dere er påmeldt
- Derfor den faste inndelingen. Er mulig å endre, men ta kontakt med
  undervisningsass. 
- Noen som kun er med remote? Gi beskjed til Olav, han anbefaler en gruppe
  for remote-folk



---down
### Lærebok
---data-style="img: max-width: 40%"
[<img src="img/pensumbok.png" style="height:50vh">](https://leanpub.com/agiletechnicalpracticesdistilled)
[<img src="img/kanban_scrum_book.jpg" style="height:50vh">](https://www.infoq.com/minibooks/kanban-scrum-minibook/)


---notes 
- mesteparten av Agile Technical Practices Distilled er pensum, noen av de mer avanserte delene er fordypningsstoff
- online bok om Kanban og Scrum
- forelesningsnotater og noen artikler på nett
- kommer til å snakke om ting som står i boken, men prøver bevisst å gi andre
  perspektiver enn det som står i læreboken for at det skal være reelt nyttig å
  komme på forelesning


---down
### Forelesningsnotater og kode

https://git.app.uib.no/inf112/22v/lectures

---notes 
- https://bit.ly/35PMZC7https://git.app.uib.no/inf112/22v/lectures


---down
### Tema

- Kommunikasjon og samarbeid
- Prosjektorganisering
- Krav
- Utviklingspraksis, testing og debugging
- Bygg og deploy av prosjekt
- Refaktorering og vedlikehold
- Designmønstre
- Lovverk

---notes 
- Mye av det som trengs for å drive med programvareutvikling. 
- Skraper såvidt i overflaten, kunne nok laget et fag basert på hver eneste
  forelesning. 
- Også: arkitektur, logging, etikk, sikkerhet, brukbarhet, design og UX,
  kodekvalitet
- Hvordan jobbe sammen
- Tekniske verktøy for å lage større programvaresystem
- Metoder som gir god kodekvalitet

---down
### Vær aktiv

---notes
- Aktive i forelesninger
- Still spørsmål
- Kom med forslag til forbedringer
- Gi feedback til foreleser, gruppeledere og hverandre
- Hjelp medstudenter
- Jobbe jevnt
- Jeg er: Faglig oppdatert
- Jeg er: Lydhør
- Jeg er: Engasjert
- Jeg er: faglig oppdatert i forhold til hva som brukes i bransjen



---down
### Kommunikasjon
<img style="height: 60vh" src="img/communication_ok_or_not.jpg">

---notes 
- bilde av mann og dame, han står over henne
- OPPGAVE: Hva ser dere i dette bildet? Bruk noen minutter 
- Er dette en OK situasjon eller ikke?
- Bruk 1 minutt på å tolke bildet, skriv kommentar om hva du tenker eller rekk
  opp hånden
- Hvis han er leder og hun er nyansatt? Aggresjon?
- Hva er de bare dypt konsentrerte og engasjerte?
- Er de likeverdige?
- Kommunikasjon er vanskelig og krever trening
- Basisferdigheter


---down
### Men vi skal jo bare kode?

---notes 
- Vi skal lage en levende representasjon av et sett med regler i en gitt
  kontekst
- Må forstå problemområdet, kontekst og regler, altså DOMENET
- For å lage rett løsning må vi forstå hva problemet er
- Vi jobber med folk hele tiden
- For å være effektiv må vi kommunisere godt



---down

## Mye vanligere enn du tror
<img style="height: 40vh" src="img/misunderstanding.jpg">

---notes
- person A snakker om sirkler
- person B snakker om firkanter
- person C snakker om trekanter
- Eller gjør de det? Kanskje det høres ut som om de snakker om ulike ting, mens
  de egentlig prater om det samme? 
- Kanskje de diskuterer noe de egentlig er enige om?
- Eksempel: språkforvirring i prosjektet jeg nettopp har jobbet i
- svindelforsøk (fraud)
- reklamasjon (dispute)
- fraud brukes også om reklamasjon, fordi det ofte er svindel som er
  utgangspunkt for en reklamasjon. Delsystemet vi har laget har faktisk feil
  navn (ett års utvikling, og mange år før det), fraud i stedet for dispute. 
- språket vi bruker er veldig abstrakt. 
- Helt vanlig med slike misforståelser. God kommunikasjon gjør at det er lettere
  å oppdage, og håndtere denne type ting. Vi må øve på å få til god og vellykket
  kommunikasjon




---down

## Hva er vellykket kommunikasjon?
<img style="height: 30vh" src="img/understanding.jpg">

---notes 
- Meldingen betyr det samme for mottaker og avsender
- Meldingen er nyttig for mottakeren
- OPPGAVE: dele opp i grupper. bruk 4 minutter og finn et eksempel på vellykket
  kommunikasjon, et eksempel på misforståelse (hva hva som helst). Fortell
  hverandre om situasjonen. Hva var bra med den vellykkede kommunikasjonen?
  Hvordan oppdaget dere at dere hadde misforstått hverandre?
- Hvilke kommunikasjonskanaler har vi vanligvis i programvareutvikling?


---

---down
### Kommunikasjonskanaler

- Epost
- Chat
- Issue tracking
- Ansikt til ansikt
- Møter
- Telefon/video
- Tekst og figur
- Dokumentasjon og kode

---notes 
- både dokumentasjon og kode er tekst. Begge er kommunikasjonskanaler. 
- felles for all kommunikasjon og samarbeid: 


---down
### Respekt
* tid, opppmerksomhet
* mengde informasjon – unyttig informasjon
* forståelse, mangel på forståelse
---notes 
- for andres tid
- for andres oppmerksomhet
- alle blir frustrerte over ting uten nytteverdi, vær kritisk til hvem som blir
  inkludert
- tilstrekkelig mengde informasjon: nok til å forstå, ikke mer enn nødvendig
- alle er travle
- for andres forståelse 
- for andres mangel på forståelse <-- ikke for å være vanskelig


---down
### Kontekst
Sett kontekst:

* Bakgrunn? Hva vil du oppnå?
* Har du gitt nok informasjon?
    * Lenke til kode, f.eks.

---notes
- alle sliter med å forstå andre innimellom: Sett kontekst
- Hva handler kommunikasjonen om? 
- Hva er bakgrunn?
- Hva ønsker du å oppnå med kommunikasjonen? 
- Har mottaker fått nok informasjon til å kunne ta stilling til det du ønsker å
  ta opp? 
- Kjøreregler og ting å huske på for de ulike kommunikasjonskanalene, begynner
  med de muntlige



---down
### Ansikt til ansikt
<img style="height: 60vh" src="img/communication_open_relaxed.jpeg">

---notes 
- Hva ser vi i bildet? Avslappet, blide, åpne for kommunikasjon
- hva ser vi etter?
- kroppsspråk (ikkeverbalt) viktig, er du avslappet? åpen? trygg? usikker?
- øyekontakt (tillit), ansiktsmimikk
- hvilke situasjoner er typiske i arbeidssammenheng? 
- kan være sosialt eller faglig på jobb
- kunnskapsoverføring
- samarbeid
- Hva må vi være særlig oppmerksomme på? 
- Kritikk
- Uenighet og diskusjon
- Ubalanse i maktforhold mellom deltagere
- Kan bruke teknikker for å åpne for konstruktiv diskusjon og tilbakemelding
- Øvelse! Dette blir bedre med erfaring 

---down
### Hvem har rett? 

- "Den skal være stor"
- "Den skal være liten"

---notes 
- Kontekst: romfart
- Person A sier den skal være STOR
- Person B sier den skal være LITEN
- OPPGAVE: Hva kan være grunnen til en slik konflikt? (tenk fysiske objekter)
- "Det viktigste er at det er mye plass" <-- noe har stort volum
- "Det viktigste er at vekten er lav" <-- volum er uviktig, men vekten kritisk

---down
### Alle gjør antagelser
* «Har jeg forstått det riktig at ...»

---notes 
- pass på å fortelle om antagelser når dere diskuterer 
- sett kontekst
- åpen for diskusjon: åpen for å greie ut misforståelser basert på upresis
  ordbruk
- tips: bruk fraser som: "Jeg forstår det på følgende måte, korriger meg gjerne
  om jeg tar feil" 
- "Har jeg forstått det riktig at .. " 
- "Jeg oppfatter at .... , stemmer dette?" 


---down
### Møter

<img style="height: 50vh" src="img/meeting_project_board.jpeg">

---notes 
- For prosjektrelatert arbeid som må diskuteres i fellesskap
- Ansikt til ansikt (kanskje også over video)
- Spesifikt tema
- Diskusjon, planlegging, status
- Korte eller lange (feks status i teamet vs dybdediskusjon om et problem)
- Bør ikke være for lange, legg i så fall inn pauser (1,5 - 2 timer er maks uten
  pause)
- Vær kritisk med hvem som trenger å være med
- Legg møter til ettermiddag eller i forbindelse med lunsj (minst mulig
  avbrytelser)
- Er alle tilstede i rommet? Hvis remote, kamera eller bare lyd?
- Interaktivt (hvis ikke, er møte rett format?)
- Hold konstruktiv tone (den som kaller inn bør ta ansvar for det, men gjør det hvis
  ingen andre gjør dette)
- forklar kontekst så kort som mulig
- Agenda: ALLTID
- Må noen forberede? Gi dem tid til det. Respekter andres tid. 
- Etter møtet: Referat hvis nødvendig. Veldig kort bakgrunn/sett kontekst. Hva ble bestemt?
  Hvem skal gjøre evt oppgaver? Hva er tidsfrist? Når skal neste aktivitet
  skje?



---down
Kroppsspråk
<img style="height: 60vh" src="img/body_language.png">



---notes 
- Bilde av to stykker som sitter sammen
- OPPGAVE: hvordan tolker dere kroppsspråket til figurene på bildet?
- ikke invader andres personlige område
- bakoverlent med kryssede armer/ben: negativ
- fremoverlent antyder interessert
- skuldre: høye, lave?
- nervøs?
- avslappet?



---down
### Telefon og video
<img style="height: 60vh" src="img/video_meeting.jpeg">

---notes 
- En til en eller mange
- I tilrettelagt møterom, fokusrom eller ved pulten
- Bruk headset med mikrofon, ikke pc-høyttaler/mikrofon
- Artikuler tydelig, forsikre deg om at folk hører deg godt nok
- Hvis mange: mute når du ikke snakker
- Sørg for god lyd (ekstern høyttaler)
- Snakk om hvordan slike møter fungerer, prøv å begrense forstyrrelser og
  problemer
- Eksempel: remote prosjekt NDLA, utviklere over hele landet
- Eksempel: jobber mot team i Trondheim, mye møter remote


---down
### Trygg kommunikasjon
* Trygg for å være effektiv
* Les kroppspråk og stemning?
* Kreativt yrke – du må være avslappet og komfortabel!
---notes
- uavhengig av hvilken måte vi samarbeider på, må vi være trygge for å klare å
  være effektive
- vanskelig å lese kroppsspråk over video, og kanskje ikke mulig
- dårlig lyd kan gjøre det vanskelig å lese stemning hos andre
- bruk noen minutter ekstra på å sørge for at stemning er god feks i starten av
  møtet (litt prat om andre ting, evt icebreaker-øvelse)
- dere skal få prøve noen icebreaker-øvelser i gruppetimene, kanskje vi også
  prøver i forelensing. Hensikt: bli komfortable med å snakke med hverandre
  digitalt, klare å lage gode relasjoner til gruppen du jobber med
- systemutvikling er et kreativt yrke, må være avslappet og komfortabel for å
  være kreativ. 


---down
### Epost
* To, CC, BCC – aldri flere enn nødvendig
* God tittel
* Mer formelt
* Viktig, men ikke hast
---notes 
- Mottakere: kun de som trenger å være med. 
- Aktiv tilbakemelding? Til-felt. 
- Trenger å se, men ikke å gi tilbakemelding? CC-felt. 
- husk at bcc også finnes. Sender du til MANGE? Bruk bcc eller liste slik at
  ingen får oversikt over alle adressene og trykker reply-to-all ved en
  feiltakelse. 
- Tema: ett tema pr eposttråd.
- Nytt tema, selv om det er til de samme? Ny eposttråd
- Innhold: kort og konsist
- Tråder: relevant svar: svar på eposten
- Tittellinje inneholder en oppsummering og stikkord for innhold, slik at folk kan huske hva dette
  dreier seg om bare ved å se i oversiktslisten. Eks: "Din flyvning med SAS
  <dato>"
- når svaret ikke haster, men er viktig
- mer formelt enn feks chat
- kan forvente at eposter blir lest og besvart


---down
### Online chat
###### (Discord, Mattermost, Slack, Teams, etc)

* Flyktig, uformell, rask, stille – lav terskel
* men: samle viktig informasjon i Wiki e.l.
* Bruk evt. tråding hvis det er mye som foregår

---notes 
- Flyktig, som oftest uformell
- Dele informasjon og ressurser (URLer/dokumenter). NB: Er det viktig, samle
  informasjon i wiki eller et mer permanent sted
- Team-informasjon
- diskusjoner der folk kan svare når det passer, men ikke like formelt som epost
- stille diskusjoner (diskutere uten å skape støy i landskapet)
- beskjeder der og da (viktig informasjon må også gis på epost)
- plugins feks for byggestatus av prosjekt, integrasjon mot github osv
- Bruk tråding i mer formelle settinger (eks KK)

---down
### Issue tracking og prosjekttavle
###### (GitHub/GitLab, Trello, Trac, Bugzilla)
* Online – eller fysisk tavle med lapper
* Inneholder:
    * hva, hvem, hvorfor, hvordan...
    * prioriteringer, historie, avgjørelser
    
---notes 
- Viktig prosjektorganiseringsverktøy
- Eksempler: Github Project board, Trello, Jira osv. 
- Kan også være fysisk: tavle med lapper
- Formelt, brukes av både team og kunder/brukere
- Inneholder: hvilke arbeidsoppgaver finnes?
- Inneholder: Hva er status på arbeidsoppgaver?
- Inneholder: Hvem jobber med hva? 
- Inneholder: Kommentarer på pågående oppgaver
- Inneholder: Historie på kommunikasjon og valg som blir tatt på oppgaver
- Vis opensource project board på github

---down
### Tekst

---notes 
- kontrakter, juridisk bindende
- informasjon om prosjekt (kontekst til hvordan bygge)
- informasjon som ikke endres ofte (enn feks chat eller ansikt-til-ansikt)




---down
### Figurer
<img style="height: 60vh" src="img/figure.jpeg">

---notes 
- bilde av figur med personer og piler
- informasjon om domenet
- informasjon om kode
- ting å huske på
- visualisere flyt (data, arbeid, prosess)
- noen figurer er flyktige (nyttig der og da), noen er nyttig over tid
  (oversikt/tilstand)
- bruker ofte figurer for å hjelpe folk å forstå konsepter (noen mennesker
  tenker visuelt)
- ofte nyttig å tenke på konsepter på ulike måter
- Andre typer figurer:


<img style="height: 60vh" src="img/building_architecture_plan.jpeg">

---notes 
- arkitektur, detaljer/oversikt


<img style="height: 60vh" src="img/code_architecture.jpeg">
---notes 
- oversikt over kode (nyttig bruk av UML)
- oversikt over oppgaver
- brukergrensesnitt



---down
### Kode
<img style="height: 60vh" src="img/two_people_coding.jpeg">

---notes 
- forretningsregler for domenet
- beskriver realiteten
- leses av mange, lever lenge
- beskriver løsningen på problemet du prøver å løse
- mye av kurset handler om hvordan kommunisere best mulig i kode
- nå skal vi snakke mer i detalj om hvordan vi diskuterer sammen, både ansikt
  til ansikt men også via chat og andre kanaler
- Dette gjelder særlig fagdiskusjoner og når man koder sammen
 

---down
### Hvordan diskutere sammen 

---down

## Et dårlig eksempel

[https://bit.ly/2smxwW0](https://bit.ly/2smxwW0)

---notes
- uhøflig språk
- uoppmerksom
- lat
- forsvinner i telefon
- baksetesjåfør
- useriøst
- arrogant
- ikke opptatt av samarbeid
- kritiserer hverandre
- svarer ikke på spørsmål
- ingen forståelse for annen kontekst
- usaklig kritikk av kode
- usaklig forsvar av kode
- gjentagelse
- ikke noe problem en gang: men hva om dette er hverdag?


---down
### Hvordan diskutere sammen 

---down
## Et godt (bedre?) eksempel

https://bit.ly/2RFTbqh

---notes
- settingen her er at to stykker skal kode sammen, parprogrammering
- åpen for endring i oppsett
- forklarer hva han gjør
- undersøker hvordan man kan jobbe best sammen (hva slags kontekst har den du
  sitter sammen med?)
- høflig: får tastatur (ber om tastatur)
- forklarer hvordan ny laptop fungerer
- lar den andre få tenke i fred
- bytter på å jobbe
- virker kanskje vanskelig i begynnelsen, men blir effektivt over tid
- kanskje ikke like bra: holder armen på stolen til naboen (men litt suboptimalt
  oppsett med bare laptop)
- det neste eksempelet er naturlig nok et med problematisk oppførsel. NB: Dette er
  satt på spissen og samler mange uvaner i ett. 


---down
### Kommunikasjon rundt kode

---notes 
- Vi bruker mye tid på å kommunisere rundt kode
- kode er ekstremt personlig, tanke oversatt direkte til tekst
- mange tar kritikk av kode som personlig kritikk
- hvis du reagerer på noe: hvorfor? 
- hvis du reagerer på noe: hva kan være et alternativ?
- være løsningsorientert, ikke problemorientert
- partner er ikke tankelesere, forklar retningen du beveger deg i
- vær høflig, profesjonell, konstruktiv
- når den andre skriver kode: følg med
- spør om å få overta hvis du har en ide
- spør om partner vil kode hvis du har kodet lenge
- la mobil og annen laptop ligge med mindre du leter etter løsning på problem
  dere nå jobber med
- hvis du må forlate arbeidet, gi beskjed hvor lenge du forventer å bli vekke
- si ifra om du trenger en lengre pause
- forsiktig med mat, er det OK for partner?
- personlig hygiene teller, veldig tett på partner
- pass kroppsspråk
- OK å være usikker
- OK å prøve ut
- OK å være uenige, kan vente med å løse noen problemer til senere
- universelle regler, enter man koder eller prater sammen, tegner sammen osv.


---down
### Hvordan jobbe sammen i par

---notes 
- http://sedano.org/toddsedano/2017/10/24/considerate-pair-programming.html
  (både del 1, 2 og 3)
- Ha to tastatur, helst to skjermer også, likeverdig plassering av partnere
- Kan være bedre å stå
- Gi komplimenter hvis du gleder deg, si hvorfor
- Introduksjon hvis du ikke har sittet sammen med noen før
- Hva er forventninger til utfall av parprogrammering? (løse problemer i
  fellesskap, levere funksjonalitet osv)
- Hvordan har den andre det i dag? Kan vi tilpasse situasjon slik at det blir
  mest mulig effektivt?
- Eks: "Er bekymret for om featuren vi lager løser problemet vi egentlig har"
- Eks: "Gleder meg til å få hjelp med dette, for jeg har stått fast i 2 dager"
- Eks: "Du har mer erfaring enn meg, så jeg er nervøs"
- Sjekk tilstand halvveis i sesjonen, be om tilbakemelding. Vær konstruktiv!
- Ta pauser når det ikke går an å konsentrere seg
- Den som trenger mest tid setter tempo på arbeidet, begge skal være med
- Si takk på slutten av dagen!
- Hvis noe går gale: 
- Gjenta/forklar med egne ord hva problemstillingen er (kan hjelpe på
  forståelse)
- ulike ideer til hvordan løse problemet? Prøv å argumentere FOR partneren sin
- gå gjennom hvordan dere har havnet i situasjonen
- prøv å tenke ut andre løsninger
- få noen andre fra teamet til å hjelpe
- prøv begge løsninger
- Forklar hva du føler: "Når du gjør .. så oppfatter i hodet mitt ... da føler jeg.."
- Snakk om noe annet: spør hva partneren er opptatt av? (trenger ikke være
  faglig)
- Deeskaler situasjonen (pause/samtale om andre ting osv)

---down
### Lønner deg seg å jobbe sammen?

---notes 
- Pros: bedre kunnskapsdeling: to stk har dyp forståelse av problemet
- To hoder tenker bedre enn ett, særlig på komplekse problemer. Bare det å
  forklare løsningen din for andre gjør at du forstår den bedre selv. 
- løser problemet fortere totalt sett, selv om det virker som om det går tregere
  i begynnelsen
- utforsker flere måter å løse problemer på
- færre feil
- Cons: veldig ulike personligheter kan ha problemer med å jobbe sammen
- noen foretrekker å jobbe alene, er evt bare ikke vant til å jobbe sammen med
  andre
- slitsomt der og da (veldig intenst)
- ulike arbeidsmetoder (feks cowboy mot planleggeren)
- kan være lett å bli distrahert


---down
### Hva oppnår vi med god kommunikasjon?

---notes 
- effektivt samarbeid
- trygge medlemmer som sier ifra om ting som ikke er bra
- bedre løsninger
- bedre kunnskapsdeling --> lavere risiko i prosjekter
- samme forståelse for situasjon, utfordringer og løsninger


---down
### Kommunikasjon er essensielt

---notes 
- ikke snakket noe særlig om hvilket språk vi bruker, og hvilke ord (kommer
  senere)
- kommunikasjon er ikke-verbalt og verbalt, skriftlig, figurer, kode, chat
- kommunikasjon legger grunnlaget for å få til et bra resultat både daglig og
  totalt

---down
### Neste forelesning: Testing

---down
### Discord

https://discord.gg/RbCEuKM3ZJ


---notes 
- satt opp en discord-server til faget. Diskuter fag, lag
  prosjektkanal/gruppekanal. Spør meg der eller send epost. 
- bruk fullt navn som nickname og et ordentlig bilde, slik at det er lett å se
  hvem som er hvem

https://youtu.be/ZbVOF0Uk5lU?t=823
