theme: moonish
title: INF112 Innføring i systemutvikling
cr_word: INF112 22V
cr_color: #1d1f20
use_menu: true
menu_bottom: false
transition: none
css: foo.css
---down
### Figurer

---notes 
- bilde av figur med personer og piler
- informasjon om domenet
- informasjon om kode
- ting å huske på
- visualisere flyt (data, arbeid, prosess)
- noen figurer er flyktige (nyttig der og da), noen er nyttig over tid
  (oversikt/tilstand)
- bruker ofte figurer for å hjelpe folk å forstå konsepter (noen mennesker
  tenker visuelt)
- ofte nyttig å tenke på konsepter på ulike måter
- Andre typer figurer:


---down
### Arbeidsflyt
<img style="height: 50vh" src="img/figure.jpeg">

---down
### Arkitektur
<img style="height: 50vh" src="img/building_architecture_plan.jpeg">
---down
### Kode-arkitektur, brukergrensesnitt
<img style="height: 50vh" src="img/code_architecture.jpeg">
### Mega-modell: data-representasjoner og prosesser
<img style="height: 50vh" src="img/megamodel.png">
---notes 
- arkitektur, detaljer/oversikt


---notes 
- oversikt over kode (nyttig bruk av UML)
- oversikt over oppgaver
- brukergrensesnitt



---down
### Kode
<div class="columns">
<img style="width:100%" src="img/two_people_coding.jpeg">
<ul style="font-size: smaller">

<li> Forretningsregler
<li> Beskriver realiteten
<li> Lever lenge
<li> For både menneske og maskin
<li> <b>Kommunisere om kode!</b>
</ul>
</div>

---notes 
- forretningsregler for domenet
- beskriver realiteten
- leses av mange, lever lenge
- beskriver løsningen på problemet du prøver å løse
- mye av kurset handler om hvordan kommunisere best mulig i kode
- nå skal vi snakke mer i detalj om hvordan vi diskuterer sammen, både ansikt
  til ansikt men også via chat og andre kanaler
- Dette gjelder særlig fagdiskusjoner og når man koder sammen
 

---down
### Hvordan diskutere sammen – dårlig eksempel

[https://bit.ly/2smxwW0](https://bit.ly/2smxwW0)

---notes
- uhøflig språk
- uoppmerksom
- lat
- forsvinner i telefon
- baksetesjåfør
- useriøst
- arrogant
- ikke opptatt av samarbeid
- kritiserer hverandre
- svarer ikke på spørsmål
- ingen forståelse for annen kontekst
- usaklig kritikk av kode
- usaklig forsvar av kode
- gjentagelse
- ikke noe problem en gang: men hva om dette er hverdag?


---down
### Hvordan diskutere sammen – godt (bedre?) eksempel

https://bit.ly/2RFTbqh

---notes
- settingen her er at to stykker skal kode sammen, parprogrammering
- åpen for endring i oppsett
- forklarer hva han gjør
- undersøker hvordan man kan jobbe best sammen (hva slags kontekst har den du
  sitter sammen med?)
- høflig: får tastatur (ber om tastatur)
- forklarer hvordan ny laptop fungerer
- lar den andre få tenke i fred
- bytter på å jobbe
- virker kanskje vanskelig i begynnelsen, men blir effektivt over tid
- kanskje ikke like bra: holder armen på stolen til naboen (men litt suboptimalt
  oppsett med bare laptop)
- det neste eksempelet er naturlig nok et med problematisk oppførsel. NB: Dette er
  satt på spissen og samler mange uvaner i ett. 


---down
### Kommunikasjon rundt kode
<img style="height: 50vh" src="img/close-pair.png">
---notes 
- Vi bruker mye tid på å kommunisere rundt kode
- kode er ekstremt personlig, tanke oversatt direkte til tekst
- mange tar kritikk av kode som personlig kritikk
- hvis du reagerer på noe: hvorfor? 
- hvis du reagerer på noe: hva kan være et alternativ?
- være løsningsorientert, ikke problemorientert
- partner er ikke tankelesere, forklar retningen du beveger deg i
- vær høflig, profesjonell, konstruktiv
- når den andre skriver kode: følg med
- spør om å få overta hvis du har en ide
- spør om partner vil kode hvis du har kodet lenge
- la mobil og annen laptop ligge med mindre du leter etter løsning på problem
  dere nå jobber med
- hvis du må forlate arbeidet, gi beskjed hvor lenge du forventer å bli vekke
- si ifra om du trenger en lengre pause
- forsiktig med mat, er det OK for partner?
- personlig hygiene teller, veldig tett på partner
- pass kroppsspråk
- OK å være usikker
- OK å prøve ut
- OK å være uenige, kan vente med å løse noen problemer til senere
- universelle regler, enter man koder eller prater sammen, tegner sammen osv.


---down
### Hvordan jobbe sammen i par
<img style="height: 50vh" src="img/ping-pong.png">

---notes 
- http://sedano.org/toddsedano/2017/10/24/considerate-pair-programming.html
  (både del 1, 2 og 3)
- Ha to tastatur, helst to skjermer også, likeverdig plassering av partnere
- Kan være bedre å stå
- Gi komplimenter hvis du gleder deg, si hvorfor
- Introduksjon hvis du ikke har sittet sammen med noen før
- Hva er forventninger til utfall av parprogrammering? (løse problemer i
  fellesskap, levere funksjonalitet osv)
- Hvordan har den andre det i dag? Kan vi tilpasse situasjon slik at det blir
  mest mulig effektivt?
- Eks: "Er bekymret for om featuren vi lager løser problemet vi egentlig har"
- Eks: "Gleder meg til å få hjelp med dette, for jeg har stått fast i 2 dager"
- Eks: "Du har mer erfaring enn meg, så jeg er nervøs"
- Sjekk tilstand halvveis i sesjonen, be om tilbakemelding. Vær konstruktiv!
- Ta pauser når det ikke går an å konsentrere seg
- Den som trenger mest tid setter tempo på arbeidet, begge skal være med
- Si takk på slutten av dagen!
- Hvis noe går gale: 
- Gjenta/forklar med egne ord hva problemstillingen er (kan hjelpe på
  forståelse)
- ulike ideer til hvordan løse problemet? Prøv å argumentere FOR partneren sin
- gå gjennom hvordan dere har havnet i situasjonen
- prøv å tenke ut andre løsninger
- få noen andre fra teamet til å hjelpe
- prøv begge løsninger
- Forklar hva du føler: "Når du gjør .. så oppfatter i hodet mitt ... da føler jeg.."
- Snakk om noe annet: spør hva partneren er opptatt av? (trenger ikke være
  faglig)
- Deeskaler situasjonen (pause/samtale om andre ting osv)

---down
### Lønner deg seg å jobbe sammen?
<img style="height: 50vh" src="img/lone-wolf.gif">

---notes 
- Pros: bedre kunnskapsdeling: to stk har dyp forståelse av problemet
- To hoder tenker bedre enn ett, særlig på komplekse problemer. Bare det å
  forklare løsningen din for andre gjør at du forstår den bedre selv. 
- løser problemet fortere totalt sett, selv om det virker som om det går tregere
  i begynnelsen
- utforsker flere måter å løse problemer på
- færre feil
- Cons: veldig ulike personligheter kan ha problemer med å jobbe sammen
- noen foretrekker å jobbe alene, er evt bare ikke vant til å jobbe sammen med
  andre
- slitsomt der og da (veldig intenst)
- ulike arbeidsmetoder (feks cowboy mot planleggeren)
- kan være lett å bli distrahert


---down
### Hva oppnår vi med god kommunikasjon?
<img style="height: 50vh" src="img/rubber-duck.gif">

---notes 
- effektivt samarbeid
- trygge medlemmer som sier ifra om ting som ikke er bra
- bedre løsninger
- bedre kunnskapsdeling --> lavere risiko i prosjekter
- samme forståelse for situasjon, utfordringer og løsninger


---down
### Kommunikasjon er essensielt

---notes 
- ikke snakket noe særlig om hvilket språk vi bruker, og hvilke ord (kommer
  senere)
- kommunikasjon er ikke-verbalt og verbalt, skriftlig, figurer, kode, chat
- kommunikasjon legger grunnlaget for å få til et bra resultat både daglig og
  totalt

---down
### Testing

<div class="columns" style="width:100%;grid-template-columns: 10% 90%;align-items:center;">
<img style="display:block;max-width:200px !important" src="img/donald-knuth@2x.jpg">
<div>
<q>Beware of bugs in the above code; I have only proved it correct, not tried it.</q>
<p style="text-align:right;font-style:italic;font-size: smaller">– Donald Knuth
</div>
</div>

---down
### Former for testing

Unit, Integration, Regression, Functional, Acceptance, Accesibility, Performance, Security, …

Automatisk? Interaktiv? Med utvikler? QA team? Brukere?

Hvor trygg kan du være? 